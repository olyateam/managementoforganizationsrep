namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableOrganizationId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.Employees", new[] { "OrganizationId" });
            AlterColumn("dbo.Employees", "OrganizationId", c => c.Int());
            CreateIndex("dbo.Employees", "OrganizationId");
            AddForeignKey("dbo.Employees", "OrganizationId", "dbo.Organizations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.Employees", new[] { "OrganizationId" });
            AlterColumn("dbo.Employees", "OrganizationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "OrganizationId");
            AddForeignKey("dbo.Employees", "OrganizationId", "dbo.Organizations", "Id", cascadeDelete: true);
        }
    }
}
