namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotNullOrganizationFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Organizations", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Organizations", "Address", c => c.String(nullable: false));
            AlterColumn("dbo.Organizations", "PhoneNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Organizations", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Organizations", "Email", c => c.String());
            AlterColumn("dbo.Organizations", "PhoneNumber", c => c.String());
            AlterColumn("dbo.Organizations", "Address", c => c.String());
            AlterColumn("dbo.Organizations", "Name", c => c.String());
        }
    }
}
