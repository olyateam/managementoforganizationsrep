namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrganizationIdMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "Organization_Id", "dbo.Organizations");
            DropIndex("dbo.Employees", new[] { "Organization_Id" });
            RenameColumn(table: "dbo.Employees", name: "Organization_Id", newName: "OrganizationId");
            AlterColumn("dbo.Employees", "OrganizationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "OrganizationId");
            AddForeignKey("dbo.Employees", "OrganizationId", "dbo.Organizations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "OrganizationId", "dbo.Organizations");
            DropIndex("dbo.Employees", new[] { "OrganizationId" });
            AlterColumn("dbo.Employees", "OrganizationId", c => c.Int());
            RenameColumn(table: "dbo.Employees", name: "OrganizationId", newName: "Organization_Id");
            CreateIndex("dbo.Employees", "Organization_Id");
            AddForeignKey("dbo.Employees", "Organization_Id", "dbo.Organizations", "Id");
        }
    }
}
