namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhoneNumberString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Organizations", "PhoneNumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Organizations", "PhoneNumber", c => c.Int(nullable: false));
        }
    }
}
