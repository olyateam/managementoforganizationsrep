namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganizationPhoneLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Organizations", "PhoneNumber", c => c.String(nullable: false, maxLength: 16));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Organizations", "PhoneNumber", c => c.String(nullable: false));
        }
    }
}
