﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Core.Entities;

namespace Core.Contexts
{
    public class ManagementOfOrganizationsContext : DbContext
    {
        public ManagementOfOrganizationsContext(): base("DefaultConnectionString")
        {
        }

        public DbSet<Organization> Organizations { get; set; }

        public DbSet<Employee> Employees { get; set; }
    }
}
