﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class Employee
    {
        public int Id { get; set; }

        [Display(Name="Имя")]
        [Required(ErrorMessage = "Укажите имя")]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Укажите фамилию")]
        public string Surname { get; set; }

        [Display(Name = "Отчество")]
        [Required(ErrorMessage = "Укажите отчество")]
        public string Middlename { get; set; }

        [Display(Name = "Дата рождения")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }

        [Display(Name = "Организация")]
        public int? OrganizationId { get; set; }

        public virtual Organization Organization { get; set; }

        [Display(Name = "Должность")]
        public string Position { get; set; }
    }
}
