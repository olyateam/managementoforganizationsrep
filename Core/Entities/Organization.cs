﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class Organization
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Название обязательное поле")]
        public string Name { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Адрес обязательное поле")]
        public string Address { get; set;  }

        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Номер телефона обязательное поле")]
        [StringLength(16, ErrorMessage = "Номер телефона должен быть 11 цифр", MinimumLength = 16)]
        [RegularExpression(@"^\+7\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Неверный номер телефона")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Некорректный email")]
        [Required(ErrorMessage = "Email обязательное поле")]
        public string Email { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }

        public Organization()
        {
            Employees = new List<Employee>();
        }
    }
}
