﻿using System.Web;
using System.Web.Optimization;

namespace WebSite
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            /*bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));*/

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/bootstrap/js/bootstrap.js",
                        "~/bootstrap/js/bootstrap-collapse.js",
                        "~/bootstrap/js/bootstrap-dropdown.js",
                        "~/bootstrap/js/bootstrap-datepicker.js",
                        "~/bootstrap/js/bootstrap-datepicker.ru.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/inputmask").Include(
            "~/Scripts/jquery.inputmask.bundle.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/script.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/employee").Include(
                        "~/Scripts/employee.js"));
            bundles.Add(new StyleBundle("~/Content/Styles").Include("~/Content/Styles/site.css"));
            bundles.Add(new StyleBundle("~/bootstrap/css").Include(
                "~/bootstrap/css/bootstrap.css",
                "~/bootstrap/css/bootstrap-datepicker.css"));
        }
    }
}