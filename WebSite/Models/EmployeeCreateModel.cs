﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Entities;

namespace WebSite.Models
{
    public class EmployeeCreateModel
    {
        public Employee Employee { get; set; }

        public IEnumerable<SelectListItem> Organizations { get; set; }
    }
}